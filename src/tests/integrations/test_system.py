import pytest


@pytest.mark.anyio
async def test_get_ready_health_check(api_client):
    response = api_client.get("/health/ready")
    assert response.status_code == 200
    assert response.json() == {"status": "ok"}


@pytest.mark.anyio
async def test_get_up_health_check(api_client):
    response = api_client.get("/health/up")
    assert response.status_code == 200
    assert response.json() == {"status": "ok"}
