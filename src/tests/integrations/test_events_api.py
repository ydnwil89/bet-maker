import pytest

from src.entities import EventStatus, generate_id
from src.repositories import BetRepository


@pytest.mark.anyio
async def test_change_events(api_client, bet):
    """Измнение события"""
    bet_repo = BetRepository()
    bet.event_id = generate_id()
    with api_client.app.container.bet_repo.override(bet_repo):
        await bet_repo.delete_all()
        await bet_repo.create_bet(bet)

        response = api_client.put(
            "/events",
            data={
                'event_id': bet.event_id,
                'new_status': EventStatus.WIN,
            }
        )

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert bet_repo.get_all(bet.event_id) == []
    assert response.json() == [
        {
            'bet_id': '1111-1111-1111-0001',
            'event_id': '0000-0000-0000-0000',
            'first_contender_win': True,
            'bet_win': None,
        }
    ]
