import pytest

from src.repositories import BetRepository


@pytest.mark.anyio
async def test_get_bets_empty(api_client, bet_repo):
    """Получение списка ставок (хранилище ставок пустое)"""
    await bet_repo.delete_all()

    response = api_client.get("/bets", params={'event_id': '100'})

    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.anyio
async def test_get_bets_empty_by_wrong_event_id(api_client, bet_repo, bet):
    """Получение пустого списка ставок (ставок с таким event_id нет в системе)"""
    bet_repo = BetRepository()
    with api_client.app.container.bet_repo.override(bet_repo):
        await bet_repo.delete_all()
        await bet_repo.create_bet(bet)

        response = api_client.get("/bets", params={'event_id': '0000-0000-0000-0001'})

    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.anyio
async def test_get_bets(api_client, bet):
    """Получение списка ставок"""
    bet_repo = BetRepository()
    with api_client.app.container.bet_repo.override(bet_repo):
        await bet_repo.delete_all()
        await bet_repo.create_bet(bet)

        response = api_client.get("/bets", params={'event_id': '0000-0000-0000-0000'})

    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json() == [
        {
            'bet_id': '1111-1111-1111-0001',
            'event_id': '0000-0000-0000-0000',
            'first_contender_win': True,
            'bet_win': None,
        }
    ]
