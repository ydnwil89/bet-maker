import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient

from src.service import create_app


@pytest.fixture()
async def api_app():
    app = FastAPI()
    app = await create_app(app)
    return app


@pytest.fixture
def api_client(api_app):
    http_client = TestClient(api_app)
    return http_client

