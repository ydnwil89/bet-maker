import pytest

from src.entities import Bet
from src.repositories import BetRepository


@pytest.fixture
def bet_repo():
    return BetRepository()


@pytest.fixture
def bet():
    return Bet(
        bet_id='1111-1111-1111-0001',
        event_id='0000-0000-0000-0000',
        first_contender_win=True,
    )
