from fastapi import APIRouter

router = APIRouter()


@router.get('/health/up', tags=['system'])
async def up():
    """HTTP обработчик для /up хелсчека"""
    return {"status": "ok"}


@router.get('/health/ready', tags=['system'])
async def ready():
    """HTTP обработчик для /ready хелсчека"""
    return {"status": "ok"}


