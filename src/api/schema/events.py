from pydantic import BaseModel

from src.entities import EventStatus


class ChangeEventSchemaRequest(BaseModel):
    event_id: str
    new_status: EventStatus
