from pydantic import BaseModel, Field

from src.entities import generate_id


class CreateBetSchemaRequest(BaseModel):
    """Схема апи для создания ставки"""

    bet_id: str = Field(default_factory=generate_id)
    event_id: str
    amount: int
    first_contender_win: bool
