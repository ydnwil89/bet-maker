from dependency_injector import containers, providers

from src.logic.services import BetService
from src.repositories import BetRepository


class IocContainer(containers.DeclarativeContainer):
    bet_repo = providers.Singleton(
        BetRepository,
    )
    bet_service = providers.Factory(BetService, bet_repository=bet_repo)
