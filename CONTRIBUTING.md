
# Dev установка

### Установка зависимостей


```sh
poetry install --with dev
```

### Запуск

```sh
poetry shell
cp .env.example .env
export $(cat .env | xargs) && python main.py
```


## Запуск тестов

```sh
poetry shell
PYTHONPATH="${PYTHONPATH}:." pytest
```
